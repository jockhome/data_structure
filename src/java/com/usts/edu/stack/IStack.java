package com.usts.edu.stack;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/28
 * Description :栈的JAVA表示，先进后出，后进先出
 * Version :1.0
 */
public interface IStack {
//    置空栈
    public void clear();

    public boolean isEmpty();// 判断是否为空，top=0；

    public int length();// length = top

    public Object peek();//读取栈内元素，并返回值，若为空则返回null

    public void push(Object x) throws Exception;//入栈操作，将x元素压入栈顶

    public Object pop();// 删除并返回栈顶元素

    public void display();
}
