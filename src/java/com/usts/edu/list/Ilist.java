package com.usts.edu.list;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/27
 * Description : 线性表
 * Version :1.0
 */
public interface Ilist {
    /**
     * 顺序表的特点
     * 1. 在线性表中逻辑上相邻的元素在物理存储位置上也是相邻的。
     * 2. 存储密度高：存储密度=数据元素原本需要的存储空间/实际占用空间
     * 3. 便于随机存取
     * 4。不便于插入和删除操作，插入删除操作会引起大量数据元素的移动
     */

//    将已存在的线性表置空
    public void clear();

//    判断是否为空
    public boolean isEmpty();

//    求线性表中数据元素个数并返回其值
    public int length();

//    读取第i个元素的值 区间：[0，length()-1]
    public Object get(int i) throws Exception;

//    插入元素，i表示插入的地址取值 区间：[0，length()-1]，当为length()-1时，表示在表尾插入x
    public void insert(int i,Object x) throws Exception;

//    删除位于第i个的元素
    public void remove(int i) throws Exception;

//    返回线性表中元素第一次出现的index
    public int indexOf(Object x) throws Exception;

//    输出线性表中各个数据元素的值
    public void display();


}
