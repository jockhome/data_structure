package com.usts.edu.stack;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/28
 * Description : 借助顺序栈将一个数组中元素倒置
 * Version :1.0
 */
public class StuckReverse {

    public static void main(String[] args) throws Exception{
        Object[] arr = new Object[10];
        for (int i = 0; i <arr.length ; i++) {
            arr[i] = i;
        }
//        reverse(arr);
        System.out.println(isPalindSeq("124321"));
        System.out.println(isPalindSeq("1234321"));

    }
//    通过栈倒置，利用先进后出机制
    public static void reverse(Object[] arr) throws Exception{


        SqlStack sqlStack = new SqlStack(arr.length);
        for (int i = 0; i <arr.length ; i++) {
            sqlStack.push(arr[i]);// 压入栈
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sqlStack.pop();
        }
        for (Object o:
            arr ) {
            System.out.println(o.toString());
        }
    }
    // 通过栈判断是否是回文数
    public static boolean isPalindSeq(String str) throws Exception{
        char[] chars = str.toCharArray();
        SqlStack sqlStack = new SqlStack(chars.length);

        for (int i = 0; i <chars.length ; i++) {
            sqlStack.push(chars[i]);
        }
        for (int i = 0; i <chars.length ; i++) {
            if (!sqlStack.pop().equals(chars[i])){
                return false;
            }
        }
        return true;
    }
}
