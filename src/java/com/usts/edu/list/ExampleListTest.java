package com.usts.edu.list;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/27
 * Description : 线性表测试
 * Version :1.0
 */
public class ExampleListTest {

    public static void main(String[] args) throws Exception{
        SqList sqList = new SqList(10);
        sqList.insert(0,0);
        sqList.insert(1,1);
        sqList.insert(2,2);
        sqList.insert(3,3);
        sqList.insert(4,4);
        System.out.println(sqList.length());
//        sqList.display();
//        sqList.remove(2);
        sqList.insert(2,7);
//        System.out.println(sqList.length());

//        sqList.display();
        sqList.remove(4);
        sqList.display();
    }
}
