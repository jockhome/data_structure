package com.usts.edu.string;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/28
 * Description : 串的接口
 * Version :1.0
 */
public interface IString {

    public void clear();//将一个已经存在的串置空

    public boolean isEmpty();// 根据串长进行判断，如果为0则为空，上述clear()则是将串置空方法 curlen = 0;

    public int length(); // 根据curlen判断串长

    public char charAt(int index); // 根据指针获取某位置的元素 区间[0,length()-1]

    public IString substring(int begin,int end); // 根据头尾指针切割串,begin & end 区间 [0,length-1].[1,length()]

    public IString insert(int offset,IString str); // 从某个字符插入串，offset区间[0,length()] 因为可以尾巴插入

    public IString delete(int begin,int end); // 从[begin,end] 区间删除串，begin & end 区间 [0,length-1].[1,length()]

    public IString concat(IString str); //把str链接在当前串后

    public int compareTo(IString str); // 当前串和目标串进行比较，当前串 > 目标串 则返回 1，= 返回 0, < 返回-1；

    public int indexOf(IString str,int begin); //从begin搜索非空串str，目标存在则返回index，不存在则返回-1


}
