package com.usts.edu.list;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/27
 * Description :单链表
 * Version :1.0
 */
public class Node {
    /**
     * 采用链式存储方式存储的线性表成为单链表
     * 1. 链表中每一个结点包含存放数据元素值的数据域和存放指向逻辑上相邻结点的指针域
     * 2. 若节点中只包含一个指针域，则称此链表为单链表
     */
    public Object data;//存放结点值

    public Node next; // 存放逻辑指针

    public Node(){
        this(null,null);// 置空
    }
//  两个参数的单链表
    public Node(Object data, Node next) {
        this.data = data;
        this.next = next;
    }

    // 带一个参数的单链表
    public Node(Object data){
        this(data,null);
    }
}
