package com.usts.edu.tree;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/29
 * Description : 二叉树的链式存储结构
 * Version :1.0
 */
public class BiTreeNode {

    public Object data;

    public BiTreeNode lchild,rchild; // 左右孩子

    public BiTreeNode(){ // 空构造函数
        this(null);
    }

    public BiTreeNode(Object data){
        this(data,null,null);
    }

    public BiTreeNode(Object data,BiTreeNode lchild,BiTreeNode rchild){ // 赋值

        this.data = data;
        this.lchild = lchild;
        this.rchild = rchild;

    }
}
