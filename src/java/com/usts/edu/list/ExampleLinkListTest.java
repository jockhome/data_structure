package com.usts.edu.list;

import java.util.Scanner;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/27
 * Description : 测试单链表
 * Version :1.0
 */
public class ExampleLinkListTest {

    public static void main(String[] args) throws Exception {
        int len = 10;
        LinkList linkList = new LinkList();
//        利用for生成链表
        for (int i = 0; i < len ; i++) {

            linkList.insert(i,i);
        }
        System.out.println("请输入查找的值");

        int i = new Scanner(System.in).nextInt();
        if (i>0&&i<=len) System.out.println("第"+i+"个元素的直接前驱"+linkList.get(i-1));
        linkList.display();
        linkList.clear();
        linkList.display();
        linkList.insert(0,1);
        linkList.insert(1,21);
        linkList.display();
        linkList.remove(0);
        linkList.display();
    }
}
