package com.usts.edu.tree;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/29
 * Description :树的生成遍历测试类
 * Version :1.0
 */
public class TreeTest {
    public static void main(String[] args) {

        BiTree tree = createBiTree();
        BiTreeNode root = tree.getRoot();
        tree.postRootTraverse(root);
        tree.preRootTraverse(root);
    }
    public static BiTree createBiTree(){
        BiTreeNode d = new BiTreeNode("D");
        BiTreeNode g = new BiTreeNode("G");
        BiTreeNode h = new BiTreeNode("H");
        BiTreeNode e = new BiTreeNode("E",g,null);
        BiTreeNode b = new BiTreeNode("B",d,e);
        BiTreeNode f = new BiTreeNode("F",null,h);
        BiTreeNode c = new BiTreeNode("C",f,null);
        BiTreeNode a = new BiTreeNode("A",b,c);
        return new BiTree(a);
    }
}
