package com.usts.edu.Queue;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/29
 * Description : 队列 接口
 * Version :1.0
 */
public interface IQueue {

    public void clear(); // 置空

    public boolean isEmpty(); //判空，front = null

    public int length(); // 返回计数 length

    public Object peek(); // 读取队首元素

    public void offer(Object x) throws Exception; // 插入新的元素，使其成为新的队尾元素

    public Object poll();// 出队操作


}
