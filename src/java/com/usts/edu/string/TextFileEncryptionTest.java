package com.usts.edu.string;

import java.util.Scanner;

/**
 * Created by Guanzhong Hu
 * Date :2020/1/30
 * Description :
 * Version :1.0
 */
public class TextFileEncryptionTest {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        SeqString originalfilename, encryptedfilename;
        int key;   //密钥
        TextFileEncryption textfile = new TextFileEncryption();
        System.out.println("文本文件加密解密演示程序");
        System.out.println("请选择？（1- 加密 2-解密）：");
        int select = scanner.nextInt();
        if (select == 1) {                                      //加密
            System.out.println("请输入需要被加密的文本文件名：");
            originalfilename = new SeqString(scanner.next());
            System.out.println("请输入加密后的文本文件名：");
            encryptedfilename = new SeqString(scanner.next());
            System.out.println("请输入加密密钥：");
            key = scanner.nextInt();
            textfile.encryptFile(originalfilename, encryptedfilename, key);
            System.out.println("加密成功！文件：" + originalfilename + " 已被加密为：" + encryptedfilename);
        } else {                                               //解密
            System.out.println("请输入需要被解密的文本文件名：");
            encryptedfilename = new SeqString(scanner.next());
            System.out.println("请输入解密后的文本文件名：");
            originalfilename = new SeqString(scanner.next());
            System.out.println("请输入解密密钥：");
            key = scanner.nextInt();
            textfile.decryptFile(encryptedfilename, originalfilename, key);
            System.out.println("解密成功！文件：" + encryptedfilename + " 已被解密为：" + originalfilename);
        }
    }
}
