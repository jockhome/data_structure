#排序

####排序的定义
1. 将一组无序的记录调整成有序的记录。

####排序的分类
######1. 内部排序和外部排序
按照排序的过程中所涉及的存储器的不同分为内排序和外排序。
内排序是指待排序的序列完全存放在内存中进行排序，这种方法不适合数量大的数据元素进行排序。
外排序是指待排序数据量大，需要存储在外部存储器中，排序过程需要访问外部存储器。
######2. 稳定排序和不稳定排序
对任意一组数据元素序列，使用某种排序算法对它按照关键字排序，若相同关键字间的前后位置关系在排序前后保持一致，则称此排序方法是稳定的，不能保持一致则是不稳定的。
