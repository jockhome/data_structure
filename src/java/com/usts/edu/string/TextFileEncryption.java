package com.usts.edu.string;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Guanzhong Hu
 * Date :2020/1/30
 * Description :对文件进行加密
 * Version :1.0
 */
public class TextFileEncryption extends SeqString {
    //对明码文件按照密钥加密后形成密码文件

    public void encryptFile(SeqString originalfilename, SeqString encryptedfilename, int key) throws IOException //从指定文本文件中读取字符串
    {
        FileReader fin = new FileReader(originalfilename.toString());
        BufferedReader bin = new BufferedReader(fin);
        FileWriter fout = new FileWriter(encryptedfilename.toString());
        SeqString encryptedline;
        String aline;
        SeqString textline;     //一行文本
        do {
            aline = bin.readLine();     //读取一行字符串，输入流结束时返回null
            if (aline != null) {
                textline = new SeqString(aline);
                encryptedline = jiaMi(textline, key);   //加密当前行
                fout.write(encryptedline.toString() + "\r\n");  //写入文件
            }
        } while (aline != null);
        bin.close();
        fin.close();
        fout.close();
    }
    ////对密码文件按照密钥解密后形成明码文件

    public void decryptFile(SeqString encryptedfilename, SeqString originalfilename, int key) throws IOException //从指定文本文件中读取字符串
    {
        FileReader fin = new FileReader(encryptedfilename.toString());
        BufferedReader bin = new BufferedReader(fin);
        FileWriter fout = new FileWriter(originalfilename.toString());
        SeqString decryptedline;
        String aline;
        SeqString textline;     //一行文本
        do {
            aline = bin.readLine();       //读取一行字符串，输入流结束时返回null
            if (aline != null) {
                textline = new SeqString(aline);
                decryptedline = jieMi(textline, key);        //解密当前行
                fout.write(decryptedline.toString() + "\r\n");  //写入文件
            }
        } while (aline != null);
        bin.close();
        fin.close();
        fout.close();
    }

    //加密一个字符串
    public SeqString jiaMi(SeqString s, int key) {
        SeqString str = new SeqString();
        int ch;
        for (int i = 0; i < s.length(); i++) {
            ch = s.charAt(i) ^ (((int) Math.sqrt(key)) % 126 + 1);  //加密
            if (ch == 13) {
                ch = ch + 1;        //回车符特殊处理
            }
            if (ch == 10) {
                ch = ch + 1;        //换行符特殊处理
            }
            str.concat((char) ch);
        }
        return str;

    }
    //解密一个字符串

    public SeqString jieMi(SeqString s, int key) {
        SeqString str = new SeqString();
        int ch, temp;
        for (int i = 0; i < s.length(); i++) {
            temp = s.charAt(i);
            if (temp == 14) {
                temp = temp - 1;   //回车符特殊处理
            }
            if (temp == 11) {
                temp = temp - 1;   //换行符特殊处理
            }
            ch = temp ^ (((int) Math.sqrt(key)) % 126 + 1);  //解密
            str.concat((char) ch);
        }
        return str;
    }
}
