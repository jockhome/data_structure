package com.usts.edu.sort.internal;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/28
 * Description : 快速排序 内排序
 * 最坏 O(n^2)
 * AVG O(nlog2n)
 * Version :1.0
 */
//TODO
public class QuickSort {
    /**
     * 1. 定义分界值，通过分界值将数组分为左右两部分
     * 2. 大于分界值得放在右边，小于分界值放在左边
     * 3. 左右两边单独排序，递归进行
     * 案例 5 4 3 1 6 3 9
     * 分界值 5： 4 3 1 3 5 6 9
     * 芬姐值 3： 1 3 3 4 5 6 9
     * over
     */

//    public int
}
